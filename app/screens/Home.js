import React from 'react';
import { View, StatusBar } from 'react-native';

import { Container } from '../components/Container';
import { Logo } from '../components/Logo';
import { LoginForm } from '../components/LoginForm';

const Home = () => (
  <Container>
    <Logo />
    <LoginForm />
  </Container>
);

export default Home;
