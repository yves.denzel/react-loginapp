import React, { Component } from 'react';

import { View, TextInput, Text, TouchableOpacity, KeyboardAvoidingView, Button } from 'react-native';
import styles from './styles';


class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      emailErrors: [],
      password: '',
      passwordErrors: [],
      disabled: false,
    };
  }

  onChangeEmailValidate = (text) => {
    this.setState({ email: text }); // set input state
    const errorText = 'not correct email format for email address';
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(text)) {
      this.setState({ // add the error to error list and update the state
        errors: [...this.state.errors, errorText],
        emailError: errorText,
      });
    } else {
      this.setState({ // remove the error from error list and update the state
        errors: this.state.errors.splice(this.state.errors.indexOf(errorText, 1)),
        emailError: '',
      });
    }

    if (this.state.errors.length === 0) {
      this.setState({disabled: false});
    }
  };

  onChangePasswordValidate = (text) => {
    this.setState({password: text}); // set password state
    const errorText = 'password must be at least 6 characters';

    if (text.length < 6) {
      this.setState({ // add the error to error list and update the state
        errors: [...this.state.errors, errorText],
        passwordError: 'password must be at least 6 characters',
      });
    } else {
      this.setState({ // remove the error from error list and update the state
        errors: this.state.errors.splice(this.state.errors.indexOf(errorText, 1)),
        passwordError: '',
      });
    }
    if (this.state.errors.length === 0) {
      this.setState({disabled: false});
    }
  };

  validateInput = (text, type) => {
    let errors = 0;

    if (type === 'email') {
      this.setState({ email: text });
      console.log(this.state.email);
      if (this.state.email.length === 0) {
        // errors += 1;
        this.setState({ emailError: 'email is required' }, function () {
          errors += 1;
        });
      } else {
        const isValidEmail = this.validateEmail(this.state.email);
        console.log('isValidEmail '+ isValidEmail);
        if (!isValidEmail) {
          errors += 1;
          this.setState({ emailError: 'not correct format for email address' });
        } else {
          this.setState({ emailError: '' });
        }
      }
    } else {
      this.setState({ password: text });

      if (this.state.password.length === 0 ) {
        errors += 1;
        this.setState({ passwordError: 'password is required'})
      } else if (this.state.password.length < 6) {
        errors += 1;
        this.setState({ passwordError: 'please use at least 6 - 12 characters' });
      }
    }

    if (errors === 0) {
      this.setState({
        disabled: false,
        emailError: '',
        passwordError: '',
      });
    } else {
      this.setState({ disabled: true });
    }
  };

  validateEmail = (text) => {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return ( re.test(text));
  };

  submitForm = () => {
    let errors = 0;
    const passMin = 6;

    if (this.state.email.length === 0) {
      errors += 1;
      console.log('email length = 0');
      this.setState({ emailError: 'email is required' });
    } else {
      const isValidEmail = this.validateEmail(this.state.email);
      console.log('isValidEmail '+ isValidEmail);
      if (!isValidEmail) {
        errors += 1;
        this.setState({ emailError: 'not correct format for email address' });
      }
    }
    if (this.state.password.length === 0) {
      errors += 1;
      this.setState({ passwordError: 'password is required' });
    } else if (this.state.password.length < passMin) {
      errors += 1;
      this.setState({ passwordError: 'please use at least 6 - 12 characters' });
    }
    if (errors === 0) {
      alert('LOGIN SUCCESSFUL');
      this.setState({
        emailError: '',
        passwordError: '',
      });
    } else {
      alert('LOGIN FAILED');
    }
  }

  render() {
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.container}>
        <Text style={styles.inputText}>Email</Text>
        <TextInput
          placeholder="Input Email address"
          returnKeyType="next"
          onSubmitEditing={() => this.passwordInput.focus()}
          onChangeText={text => this.validateInput(text, 'email')}
          keyboardType="email-address"
          autoCapitalize="none"
          autoCorrect={false}
          spellCheck={false}
          underlineColorAndroid="transparent"
          style={styles.input}
        />
        <Text style={styles.inputError}>{this.state.emailError}</Text>
        <Text style={styles.inputText}>Password</Text>
        <TextInput
          placeholder="Input Password"
          secureTextEntry
          returnKeyType="go"
          autoCapitalize="none"
          autoCorrect={false}
          onChangeText={text => this.validateInput(text, 'password')}
          underlineColorAndroid="transparent"
          ref={input => this.passwordInput = input}
          style={styles.input}
        />
        <Text style={styles.inputError}>{ this.state.passwordError }</Text>
        <TouchableOpacity
          disabled={this.state.disabled}
          onPress={this.submitForm}
          style={styles.loginButton}
        >
          <Text style={styles.loginButtonText}>Sign In</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
}

export default LoginForm;
