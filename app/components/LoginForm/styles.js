import EStyleSheet from 'react-native-extended-stylesheet';

const INPUT_HEIGHT = 48;
export default EStyleSheet.create({
  container: {
    width: '90%',
    paddingTop: 60,
    paddingBottom: 20,
    justifyContent: 'center',
  },
  input: {
    alignSelf: 'center',
    height: INPUT_HEIGHT,
    backgroundColor: '$white',
    width: '100%',
    borderRadius: 5,
    borderColor: '#9b59b6',
    borderWidth: 1,
    marginTop: 5,
    padding: 5,
    paddingBottom: 10,
    fontSize: 18,

  },
  inputText: {
    fontWeight: '400',
    fontSize: 16,
  },
  inputError: {
    fontSize: 10,
    color: '#e74c3c',
    fontStyle: 'italic',
  },
  loginButton: {
    marginTop: 10,
    backgroundColor: '#9b59b6',
    paddingVertical: 15,
    borderRadius: 5,
  },
  loginButtonText: {
    textAlign: 'center',
    color: '#FFF',
    fontWeight: '700',
    fontSize: 18,
  },
});
